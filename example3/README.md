# Example3

Examples demostrates usage of references and re-go data filtering.

references with variable
try:

command: opa eval --data data.json --data policy.rego --format pretty data.user_roles.alice[i]
output : varible i is created and set to value {"0", "1"}
explanation : data.user_roles.alice[] has two values
command : opa eval --data data.json --data policy.rego --format pretty 'data.user_roles.adam[i]'
Output : varible i is created and set to value {"0"}

without variables (_)
opa eval --data data.json --data policy.rego --format pretty 'data.user_roles.adam[_]'

creating variables with some
opa eval --data data.json --data policy.rego --format pretty 'some i; data.user_roles.adam[i]'


Data filtering 
Re-go trying to find values for references that would make entire condition true.
try:
opa eval --data data.json --input input.json --format pretty 'some user;  data.user_roles[user][_] == "guest"'
output : user is set to all users that has guest as role.



