package test

#sets allow_no_condition to true if all conditions are true
allow_no_condition := true{
    true
}

#no need to assign, default is true if all consitions are true
allow_no_condition_1{
    true
}

#sets to false
allow_no_condition_false := false{
    true
}

#allow_no_condition_undefined is not set because conditions are false. It would be undefined
#undefined
allow_no_condition_undefined{
    false
}

#set to default value if rule evaluates to undefined
default allow_no_condition_undefined_default = false
allow_no_condition_undefined_default{
    false
}

#sets allow_no_condition_int to int instead of boolean (scaler value)
allow_no_condition_int := 10{
    true
}

#multiple conditions, returns true if input user is alice, action: read, object: iam
#new line is AND, ; is also and
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.allow_multiple_conditions'
allow_multiple_conditions {
    input["user"] == "alice" ; input.action == "read"
    input.object == "iam"
}

#input user is alice
# opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.allow_multiple_conditions_undefined' 
#return undefined
allow_multiple_conditions_undefined {
    input["user"] == "bob" #false
    input.action == "read"
    input.object == "iam"
}

#rule returning set
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_set'
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_set["data1"]'
rule_returns_set = val {
    val = {"data1", "data2"}
}

#returns array
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_array'
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_array[0]'
rule_returns_array = val {
    val = ["data1", "data2"]
}

#rule returning object if user is alice
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_obj'
rule_returns_obj = val {
    input["user"] == "alice"
    val = {"data1": [1,2] , "data2": {"1", "2", "1"}}
}

#rule returning unndefined if user is alice
#opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.test.rule_returns_obj_undefined'
rule_returns_obj_undefined = val {
    input["user"] == "bob" #false (undefined)
    val = {"data1": [1,2] , "data2": {"1", "2", "1"}}
}
