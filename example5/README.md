# Example 5 : putting it all together
Final code check grants
input file contains input user and access requested.
base.rego contains base rules.
policy.rego uses base.rego and sets allow to true is use is granted access.
opa eval --data data.json --input input.json --data policy.rego --data base.rego --format pretty 'data.authz.allow'
output: true

Change user in input.json to eve (eve does not have access to iam)
opa eval --data data.json --input input.json --data policy.rego --data base.rego --format pretty 'data.authz.allow' 
output: false

## Policy testing
execute command opa test . -v

### coverage
opa test . -v --coverage --format=json
