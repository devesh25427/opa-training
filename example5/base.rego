package base


#rule returning set
#list all roles for user in input
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_role[x]' will return set
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_role["iam.admin"]' will return iam.admin
list_role[role]{
	role := data.user_roles[input.user][_] 
}
#return set of users for role in input
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_users'
list_users[users]{
	data.user_roles[users][roles] ==  input.role
}

#return list role grants for input user
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_grants'
list_grants[grants] {
	roles := list_role
	data.role_grants[x][_]
	x == roles[_]
	grants :=  data.role_grants[x][_]
}

#return object (for example purpose)

#introducing default keyworld
default list_users_obj = {"users": []}

#example below returns object
#try
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_users_obj'
#opa eval --data policy.rego --data data.json  --input input.json  --format pretty 'data.authz.list_users_obj["users"]
#change input.role to xyx shuld return default
list_users_obj = response{
	users := list_users
	response := {"users" : users} 
} 




