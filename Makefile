.PHONY: all build gen-go-proto gen ui
CURDIR:=${PWD}
EXE=
ifeq ($(OS), Windows_NT)
	EXE=.exe
endif

build-cli:
	go build -o rego$(EXE) example6/main.go