# Example2

This example shows creation of policy and accessing result/data of policy.

command : opa eval --data data.json --data policy.rego --format pretty 'data.test.x'
Output : 
[
  "iam.admin",
  "guest"
]

All results in the policy can be accessed using command
opa eval --data data.json --data policy.rego --format pretty 'data.test'

variables can be scaler or composite.

try
opa eval --data data.json --data policy.rego --format pretty 'data.test.str_var'
opa eval --data data.json --data policy.rego --format pretty 'data.test.ip_port_map[80]'



