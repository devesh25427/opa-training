package test

default allow = false

int_var := 10

str_var := "Hello"

#composite 
#object
rect_obj := {"width": 2, "height": x}

#set
set_obj := {"1", "2", "3"}

#complex var
ip_port_map := {
#array
80: ["1.1.1.1", "1.1.1.2"],
443: ["2.2.2.1"],
}

#variable refering to base document
x := data.user_roles.alice

