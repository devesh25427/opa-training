package main

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/open-policy-agent/opa/storage/inmem"
	"github.com/open-policy-agent/opa/util"

	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/rego"
)

var POLICY_PATHS map[string]string = map[string]string{"base": "base.rego", "policy": "policy.rego"}
var compiler *ast.Compiler

func init() {
	//Load and compile policies
	var modMap = map[string]string{}

	for m, f := range POLICY_PATHS {
		//fmt.Printf("Loading module : %s", "loading File %s \n", m, f)
		fmt.Printf("Loading module : %s File %s \n", m, f)
		if dat, err := os.ReadFile(f); err != nil {
			panic(err)
		} else {
			modMap[m] = string(dat)
		}
	}
	if c, err := ast.CompileModules(modMap); err != nil {
		fmt.Printf("Faile to compile module error %s \n", err.Error())
		panic(err)
	} else {
		compiler = c
	}
	fmt.Printf("Module loaded successfully \n")
}

//Load data
func load_data() []byte {
	if dat, err := os.ReadFile("data.json"); err != nil {
		fmt.Printf("Failed to read data file %s \n", "data.json")
		panic(err)
	} else {
		//fmt.Printf("Read data \n %s \n", string(dat))
		return dat
	}

}

//evaluates allow decision
func evaluate_allow(ctx context.Context, input map[string]interface{}) (res bool, err error) {
	//Load files as input data
	d := load_data()

	//Create store
	var json map[string]interface{}
	err = util.UnmarshalJSON(d, &json)
	if err != nil {
		fmt.Printf("Failed to unmarshal data Error %s \n", err.Error())
	}

	store := inmem.NewFromObject(json)

	r := rego.New(
		rego.Query("x = data.authz.allow"),
		rego.Compiler(compiler),
		rego.Store(store),
	)
	q, err1 := r.PrepareForEval(ctx)
	if err1 != nil {
		fmt.Printf("Error in evaluation Error %s ", err1.Error())
		return false, err1
	}

	results, err := q.Eval(ctx, rego.EvalInput(input))

	if err != nil {
		fmt.Printf("Error in query.Eval Error : %s\n", err.Error())
		return false, errors.New("error in evaluaton")
	} else if len(results) == 0 {
		// Handle undefined result.
		fmt.Printf("Error in query.Eval undefined result \n")
		return false, errors.New("error in result")

	} else if result, ok := results[0].Bindings["x"].(bool); !ok {
		// Handle unexpected result type.
		fmt.Printf("Error in query.Eval unexpected result \n")
		return false, errors.New("error unexpected result")
	} else {
		fmt.Printf("Success Result is %+v \n", result)
		res = result
		return
	}

}

func evaluate_roles(ctx context.Context, input map[string]interface{}) (res []string, err error) {
	// Create a new query that uses the compiled policy from above.
	d := load_data()

	//Create store
	var json map[string]interface{}
	err = util.UnmarshalJSON(d, &json)
	if err != nil {
		fmt.Printf("Failed to unmarshal data Error %s \n", err.Error())
	}
	store := inmem.NewFromObject(json)

	r := rego.New(
		rego.Query("x = data.base.list_role"),
		rego.Compiler(compiler),
		rego.Store(store),
	)
	q, err1 := r.PrepareForEval(ctx)
	if err1 != nil {
		fmt.Printf("Error in evaluation Error %s ", err1.Error())
		return nil, err1
	}

	results, err := q.Eval(ctx, rego.EvalInput(input))

	if err != nil {
		fmt.Printf("Error in query.Eval Error : %s\n", err.Error())
		return nil, errors.New("error in evaluaton")
	} else if len(results) == 0 {
		// Handle undefined result.
		fmt.Printf("Error in query.Eval undefined result \n")
		return nil, errors.New("error in result")

	}
	fmt.Printf("Result is %+v \n", results[0].Bindings["x"])

	return
}

func main() {
	ctx := context.TODO()
	input := map[string]interface{}{
		"user":   "alice",
		"action": "read",
		"type":   "iam",
	}
	if res, err := evaluate_allow(ctx, input); err != nil {
		fmt.Printf("Error in evaluate1 error %s \n", err.Error())
		return
	} else {
		fmt.Printf("Result is %v \n", res)
	}

	//Get roles
	evaluate_roles(ctx, input)
}
