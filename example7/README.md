# Example 6 : integrating with go lang
This example loads rego policies from base.rego and policy.rego
loads data from data.json

Then sends query to rego.
There are two functions
1. evaluate_allow : This function evaluated allow decision defined in policy.allow
2. evaluate_roles : This function evaluated roles for input user

To compile
run make : which will generate rego.exe on windows
./rego.exe : will print results

change input data in main function to get different results.

