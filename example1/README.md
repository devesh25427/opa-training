# Example1

This example shows referening data in opa query using opa eval command line tool.

command : opa eval --data data.json --input input.json --format pretty input.user
Output : alice

try command : opa eval --data data.json --input input.json --format pretty data.role_grants


Square brakets can also be used to access data.
Try
opa eval --data data.json --input input.json --format pretty 'input["user"]'

Array elements can be referenced using squire brackets [index] syntax. Try
opa eval --data data.json --input input.json --format pretty  'data.user_roles.alice[0]'

If a value is not found OPA returns undefined results (mostly treated a false).
opa eval --data data.json --data policy.rego --input input.json --format pretty 'data.user_roles.x'

Operators:
Equality operator (==, >, <, >= etc) return true or false
Try
opa eval --data data.json --input input.json --format pretty 'input.user == "alice"'
opa eval --data data.json --input input.json --format pretty 'input.user == "bob"'
opa eval --data data.json --input input.json --format pretty 'count(data.user_roles.alice) >=1'
opa eval --data data.json --input input.json --format pretty 'count(data.user_roles.alice) < 1'

AND operator (;)
Multiple operations can be joined together using and (;) operator.  All expression must be defined and true for entire expression to be true.
Try
opa eval --data data.json --input input.json --format pretty 'count(data.user_roles.alice) > 1 ; data.user_roles.alice[1] = "guest"'

not operator
not operator negates value of expression. not will turn true to false and, false and undefined to true.
opa eval --data data.json --input input.json --format pretty 'count(data.user_roles.alice) > 1 ; not data.user_roles.alice[1] = "guest"'
will return undefined