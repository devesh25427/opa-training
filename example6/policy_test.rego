package authz

test_alice_allowed_on_iam{
    allow with input as {"user": "alice","action": "read","type": "iam"}
} 

test_eve_not_allowed_on_iam{
    not allow with input as {"user": "eve","action": "read","type": "iam"}
} 