package main

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/open-policy-agent/opa/rego"
)

//Get roles for user
func evaluate_roles(ctx context.Context, input map[string]interface{}) (res []string, err error) {
	//Load files as input data
	paths := []string{"base.rego", "policy.rego", "data.json"}
	query, err := rego.New(
		//prepare query to read data authz.allow
		rego.Query("x = data.base.list_role"),
		rego.Load(paths, func(abspath string, info os.FileInfo, depth int) bool {
			fmt.Printf("loading file %s \n", abspath)
			//load all (return false)
			return false
		}),
	).PrepareForEval(ctx)
	if err != nil {
		fmt.Printf("Error in PrepareForEval Error : %s\n", err.Error())
		return nil, err
	}

	results, err := query.Eval(ctx, rego.EvalInput(input))

	if err != nil {
		fmt.Printf("Error in query.Eval Error : %s\n", err.Error())
		return nil, errors.New("error in evaluaton")
	} else if len(results) == 0 {
		// Handle undefined result.
		fmt.Printf("Error in query.Eval undefined result \n")
		return nil, errors.New("error in result")

	}
	fmt.Printf("Result is %+v \n", results[0].Bindings["x"])

	/*
		if res, ok := results[0].Bindings["x"].([]string); !ok {
			// Handle unexpected result type.
			fmt.Printf("Error in query.Eval unexpected result \n")
			return nil, errors.New("error unexpected result")
		} else {
			fmt.Printf("Success Result is %+v \n", res)
			return res, nil
		}
	*/
	return
}

//evaluates allow decision
func evaluate_allow(ctx context.Context, input map[string]interface{}) (res bool, err error) {
	//Load files as input data
	paths := []string{"base.rego", "policy.rego", "data.json"}
	query, err := rego.New(
		//prepare query to read data authz.allow
		rego.Query("x = data.authz.allow"),
		rego.Load(paths, func(abspath string, info os.FileInfo, depth int) bool {
			fmt.Printf("loading file %s \n", abspath)
			//load all (return false)
			return false
		}),
	).PrepareForEval(ctx)
	if err != nil {
		fmt.Printf("Error in PrepareForEval Error : %s\n", err.Error())
		return false, err
	}

	results, err := query.Eval(ctx, rego.EvalInput(input))

	if err != nil {
		fmt.Printf("Error in query.Eval Error : %s\n", err.Error())
		return false, errors.New("error in evaluaton")
	} else if len(results) == 0 {
		// Handle undefined result.
		fmt.Printf("Error in query.Eval undefined result \n")
		return false, errors.New("error in result")

	} else if result, ok := results[0].Bindings["x"].(bool); !ok {
		// Handle unexpected result type.
		fmt.Printf("Error in query.Eval unexpected result \n")
		return false, errors.New("error unexpected result")
	} else {
		fmt.Printf("Success Result is %+v \n", result)
		res = result
		return
	}

}

func main() {
	ctx := context.TODO()
	input := map[string]interface{}{
		"user":   "alice",
		"action": "read",
		"type":   "iam",
	}
	if res, err := evaluate_allow(ctx, input); err != nil {
		fmt.Printf("Error in evaluate1 error %s \n", err.Error())
		return
	} else {
		fmt.Printf("Result is %v \n", res)
	}

	//Get roles
	evaluate_roles(ctx, input)
}
