package authz

default allow = false

allow  {
	req_grant := {"action" : input.action, "type" : input.type} 
	grants := data.base.list_grants
	grants[req_grant]
}
