package test

apps_by_hostname[hostname] = app1 {
    some i
    server := data.sites[_].servers[_]
    hostname := server.hostname
    data.apps[i].servers[_] == server.name
    app1 := data.apps[i].name
}

hostnames[name] { name := data.sites[_].servers[_].hostname }